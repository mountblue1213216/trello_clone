import { Routes, Route } from "react-router-dom";

import Home from "./Componentes/Boards/Home";
import Navbar from "./Componentes/Navbar";
import Lists from "./Componentes/Lists/Lists";

function App() {
  return (
    <>
      <Navbar />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/lists/:id" element={<Lists />} />
      </Routes>
    </>
  );
}

export default App;
