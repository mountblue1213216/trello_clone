import { useEffect, useState } from "react";

import SingleCheckItem from "./SingleCheckItem";
import CheckItemApi from "../../services/checkItemApi";
import AddCheckItem from "./AddCheckItem";

const Checkitem = ({ checklistId, cardId }) => {
  const [checkitem, setCheckitem] = useState([]);

  useEffect(() => {
    getCheckitems(checklistId);
  }, []);

  function getCheckitems(checkListid) {
    CheckItemApi.getCheckItemsOnACheckList(checkListid)
      .then((res) => setCheckitem(res.data))
      .catch((err) => console.log(err));
  }

  function handleInputElement(event) {
    if (event.key === "Enter") {
      createCheckItemInAChecklist(event.target.value, checklistId);
    }
  }

  function createCheckItemInAChecklist(nameOfCheckItem, checkListId) {
    CheckItemApi.createCheckItemOnACheckList(nameOfCheckItem, checkListId)
      .then((res) => setCheckitem([...checkitem, res.data]))
      .catch((err) => console.log(err));
  }

  function deleteACheckItem(checkItemId, checklistId) {
    setCheckitem(checkitem.filter((list) => list.id !== checkItemId));

    CheckItemApi.deleteCheckItemFromACheckList(checkItemId, checklistId).catch(
      (err) => console.log(err)
    );
  }

  return (
    <>
      {checkitem.length > 0 &&
        checkitem.map((list, index) => (
          <SingleCheckItem
            key={index}
            name={list.name}
            deleteACheckItem={deleteACheckItem}
            id={list.id}
            checklistId={checklistId}
            cardId={cardId}
            state={list.state}
          />
        ))}
      <AddCheckItem handleInput={handleInputElement} />
    </>
  );
};

export default Checkitem;
