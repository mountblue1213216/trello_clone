import { useState, useEffect } from "react";
import { CircularProgress } from "@mui/material";
import BoardApi from "../../services/boardApis";

import AddBoard from "./AddBoard";
import Board from "./Board";

const Home = () => {
  const [board, setboard] = useState([]);
  const [inputVisible, setInputVisible] = useState(false);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    BoardApi.getAllBoards()
      .then((res) => {
        setboard(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, []);

  function createNewBoard(boardName) {
    BoardApi.createBoard(boardName)
      .then((res) => setboard([...board, res.data]))
      .catch((err) => console.log(err));
  }

  function handleInputElement(event) {
    if (event.key === "Enter") {
      createNewBoard(event.target.value);
      setInputVisible(false);
    }
  }

  const inputVisibility = () => {
    setInputVisible(true);
  };

  return (
    <>
      {isLoading && <CircularProgress />}
      <AddBoard
        boardInput={inputVisibility}
        inputValue={inputVisible}
        handleInput={handleInputElement}
      />
      {board.map((data, index) => (
        <Board
          key={index}
          id={data.id}
          img={data.prefs?.backgroundImage}
          name={data.name}
        />
      ))}
    </>
  );
};

export default Home;
