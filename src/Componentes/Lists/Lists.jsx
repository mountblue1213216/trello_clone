import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { CircularProgress } from "@mui/material";

import List from "./List";
import AddList from "./AddList";
import ListApi from "../../services/listApis";

const Lists = () => {
  const { id } = useParams();
  const [list, setlist] = useState([]);
  const [inputVisible, setInputVisible] = useState(false);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    ListApi.getAllLists(id)
      .then((res) => {
        setlist(res.data);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, []);

  function createNewList(listName, boardId) {
    ListApi.createNewList(listName, boardId)
      .then((res) => setlist([...list, res.data]))
      .catch((err) => console.log(err));
  }

  function handleInputElement(event) {
    if (event.key === "Enter") {
      createNewList(event.target.value, id);
      setInputVisible(false);
    }
  }

  function inputVisibility() {
    setInputVisible(true);
  }

  function deleteOneList(listId) {
    setlist(list.filter((oneList) => oneList.id !== listId));

    ListApi.deleteOneList(listId).catch((err) => console.log(err));
  }

  return (
    <>
      {isLoading && <CircularProgress />}
      <AddList
        listInput={inputVisibility}
        inputValue={inputVisible}
        handleInput={handleInputElement}
      />
      {list.map((data, index) => (
        <List
          key={index}
          name={data && data.name}
          deleteOneList={deleteOneList}
          id={data.id}
        />
      ))}
    </>
  );
};

export default Lists;
