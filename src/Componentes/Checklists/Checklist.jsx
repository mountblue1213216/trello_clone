import { useEffect, useState } from "react";
import { Modal } from "@mui/material";

import SingleChecklist from "./SingleChecklist";
import CheckListApi from "../../services/checkListApi";
import AddChecklist from "./AddChecklist";

const Checklist = ({ cardId, modal, handleClose }) => {
  const [checklist, setChecklist] = useState([]);

  useEffect(() => {
    getLists(cardId);
  }, []);

  function getLists(cardId) {
    CheckListApi.getAllCheckListsOnACard(cardId)
      .then((res) => setChecklist(res.data))
      .catch((err) => console.log(err));
  }

  function createCheckListInACard(nameOfChecklist, cardId) {
    CheckListApi.createChecklistOnACard(nameOfChecklist, cardId)
      .then((res) => setChecklist([...checklist, res.data]))
      .catch((err) => console.log(err));
  }

  function handleInputElement(event) {
    if (event.key === "Enter") {
      createCheckListInACard(event.target.value, cardId);
    }
  }

  function deleteAChecklist(checklistId) {
    setChecklist(checklist.filter((list) => list.id !== checklistId));

    CheckListApi.deleteChecklistOnACard(checklistId).catch((err) =>
      console.log(err)
    );
  }

  return (
    <>
      <Modal open={modal} onClose={handleClose}>
        <div
          style={{
            width: 500,
            height: "auto",
            backgroundColor: "white",
            margin: "auto",
            marginTop: 100,
          }}
        >
          {checklist.length > 0 &&
            checklist.map((list, index) => (
              <SingleChecklist
                key={index}
                name={list.name}
                deleteAChecklist={deleteAChecklist}
                id={list.id}
                cardId={cardId}
              />
            ))}
          <AddChecklist handleInput={handleInputElement} />
        </div>
      </Modal>
    </>
  );
};

export default Checklist;
