import { useState, useEffect } from "react";

import SingleCard from "./SingleCard";
import CardApi from "../../services/cardApis";
import AddCard from "./AddCard";

const Cards = ({ listId }) => {
  const [card, setCard] = useState([]);

  useEffect(() => {
    getCards(listId);
  }, []);

  function getCards(listId) {
    CardApi.getAllCardsInAList(listId)
      .then((res) => setCard(res.data))
      .catch((err) => console.log(err));
  }

  function deleteACardFromTheList(cardId) {
    setCard(card.filter((card) => card.id !== cardId));

    CardApi.deleteOneCard(cardId).catch((err) => console.log(err));
  }

  function createCardInAList(nameOfCard, listId) {
    CardApi.createACardInAList(nameOfCard, listId)
      .then((res) => setCard([...card, res.data]))
      .catch((err) => console.log(err));
  }

  function handleInputElement(event) {
    if (event.key === "Enter") {
      createCardInAList(event.target.value, listId);
    }
  }

  return (
    <>
      {card.map((cardData, index) => (
        <SingleCard
          key={index}
          name={cardData.name}
          deleteACardFromTheList={deleteACardFromTheList}
          id={cardData.id}
        />
      ))}
      <AddCard handleInput={handleInputElement} />
    </>
  );
};

export default Cards;
